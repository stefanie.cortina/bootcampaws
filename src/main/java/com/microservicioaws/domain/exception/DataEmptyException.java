package com.microservicioaws.domain.exception;

public class DataEmptyException extends RuntimeException{
    public DataEmptyException() {
        super();
    }
}
