package com.microservicioaws.domain.usecase;

import com.microservicioaws.domain.logic.ProductoOperations;
import com.microservicioaws.domain.logic.ProductoPersistence;
import com.microservicioaws.domain.model.Producto;

import java.util.List;

public class ProductoUseCase implements ProductoOperations{

    private final ProductoPersistence productoPersistence;

    public ProductoUseCase(ProductoPersistence productoPersistence) {
        this.productoPersistence = productoPersistence;
    }

    @Override
    public Producto getById(Long id) {
        return productoPersistence.getById(id);
    }

    @Override
    public void saveProducto(Producto producto) {
        productoPersistence.saveProducto(producto);
    }

    @Override
    public List<Producto> findAllProducto() {
        return productoPersistence.findAllProducto();
    }

    @Override
    public void update(Producto producto) {
        productoPersistence.update(producto);
    }

    @Override
    public void delete(Long id) {
        productoPersistence.delete(id);
    }
}
