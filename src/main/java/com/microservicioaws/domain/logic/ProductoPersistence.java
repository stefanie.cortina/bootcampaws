package com.microservicioaws.domain.logic;

import com.microservicioaws.domain.model.Producto;

import java.util.List;

public interface ProductoPersistence {
    Producto getById(Long id);
    void saveProducto(Producto producto);
    List<Producto> findAllProducto();
    void update(Producto producto);
    void delete(Long id);
}
