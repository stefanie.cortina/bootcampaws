package com.microservicioaws.infrastructure.persistence.mapper;

import com.microservicioaws.domain.model.Producto;
import com.microservicioaws.infrastructure.persistence.entity.ProductoEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductoEntityMapper {

    ProductoEntity toEntity(Producto producto);
    Producto toProducto(ProductoEntity productoEntity);
    List<Producto> toProductoList(List<ProductoEntity> productoEntityList);

}
