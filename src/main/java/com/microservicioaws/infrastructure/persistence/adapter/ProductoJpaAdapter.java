package com.microservicioaws.infrastructure.persistence.adapter;

import com.microservicioaws.domain.logic.ProductoPersistence;
import com.microservicioaws.domain.model.Producto;
import com.microservicioaws.infrastructure.exception.DataEmptyException;
import com.microservicioaws.infrastructure.exception.ProductNotFoundException;
import com.microservicioaws.infrastructure.persistence.entity.ProductoEntity;
import com.microservicioaws.infrastructure.persistence.mapper.ProductoEntityMapper;
import com.microservicioaws.infrastructure.persistence.repository.JpaProductoRepository;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Optional;


@RequiredArgsConstructor
public class ProductoJpaAdapter implements ProductoPersistence {

    private final JpaProductoRepository productoRepository;
    private final ProductoEntityMapper productoEntityMapper;

    @Override
    public Producto getById(Long id) {
        return productoEntityMapper.toProducto(productoRepository.findById(id)
                .orElseThrow(ProductNotFoundException::new));
    }

    @Override
    public void saveProducto(Producto producto) {
        if (producto.getNombre() == null || producto.getPrecio() == null ||
                producto.getNombre().isEmpty() || Double.isNaN(producto.getPrecio())){
            throw new DataEmptyException();
        }
        productoRepository.save(productoEntityMapper.toEntity(producto));
    }

    @Override
    public List<Producto> findAllProducto() {
        List<ProductoEntity> productoList = productoRepository.findAll();
        if (productoList.isEmpty()){
            throw new ProductNotFoundException();
        }
        return productoEntityMapper.toProductoList(productoList);
    }

    @Override
    public void update(Producto producto) {
        Long id = producto.getId();
        Optional<ProductoEntity> optionalProductoEntity = productoRepository.findById(id);

        if (optionalProductoEntity.isPresent()) {
            ProductoEntity productoEntity = optionalProductoEntity.get();
            productoEntity.setNombre(producto.getNombre());
            productoEntity.setPrecio(producto.getPrecio());
            productoRepository.save(productoEntity);
        } else {
            throw new ProductNotFoundException();
        }
    }

    @Override
    public void delete(Long id) {
        Optional<ProductoEntity> optionalProducto = productoRepository.findById(id);
        if (optionalProducto.isPresent()) {
            productoRepository.deleteById(id);
        }else{
            throw new ProductNotFoundException();
        }
    }
}
