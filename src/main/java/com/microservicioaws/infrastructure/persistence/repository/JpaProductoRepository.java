package com.microservicioaws.infrastructure.persistence.repository;

import com.microservicioaws.infrastructure.persistence.entity.ProductoEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface JpaProductoRepository extends JpaRepository<ProductoEntity,Long> {

    Optional<ProductoEntity> findById(Long idProducto);
    void deleteById(Long idProducto);
}
