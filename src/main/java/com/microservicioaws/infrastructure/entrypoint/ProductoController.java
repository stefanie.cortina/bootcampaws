package com.microservicioaws.infrastructure.entrypoint;

import com.microservicioaws.application.dto.ProductoDto;
import com.microservicioaws.application.handler.ProductoHandlerOperations;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/producto")
@RequiredArgsConstructor
public class ProductoController {

    private final ProductoHandlerOperations productoHandlerOperations;

    @Operation(summary = "Agrega un nuevo producto")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Producto Creado", content = @Content),
            @ApiResponse(responseCode = "409", description = "Producto ya existe", content = @Content)
    })
    @PostMapping("/")
    public ResponseEntity<Void> guardarProducto(@RequestBody ProductoDto productoDto) {
        productoHandlerOperations.saveProducto(productoDto);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @Operation(summary = "Obtener todos los productos")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Productos obtenidos",
                    content = @Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = ProductoDto.class)))),
            @ApiResponse(responseCode = "404", description = "Datos no encontrados", content = @Content)
    })
    @GetMapping("/")
    public ResponseEntity<List<ProductoDto>> getAllProductos() {
        return ResponseEntity.ok(productoHandlerOperations.findAllProducto());
    }

    @Operation(summary = "Obtener el producto por el identificador")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Producto encontrado",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = ProductoDto.class))),
            @ApiResponse(responseCode = "404", description = "Producto no encontrado", content = @Content)
    })
    @GetMapping("/{id}")
    public ResponseEntity<ProductoDto> getProductoById(@Parameter(description = "Identificacion del producto")
                                                                 @PathVariable(name = "id") Long idProducto) {
        return ResponseEntity.ok(productoHandlerOperations.getById(idProducto));
    }

    @Operation(summary = "Actualizar un producto existente")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Producto actualizado", content = @Content),
            @ApiResponse(responseCode = "404", description = "Producto no encontrado", content = @Content)
    })
    @PutMapping("/")
    public ResponseEntity<Void> updateProducto(@RequestBody ProductoDto productoDto, @PathVariable(name = "id") Long idProducto) {
        productoHandlerOperations.update(productoDto,idProducto);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Eliminar el producto por el identificador")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Producto eliminado", content = @Content),
            @ApiResponse(responseCode = "404", description = "Producto no encontrado", content = @Content)
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteProducto(@PathVariable Long idProducto) {
        productoHandlerOperations.delete(idProducto);
        return ResponseEntity.noContent().build();
    }
}
