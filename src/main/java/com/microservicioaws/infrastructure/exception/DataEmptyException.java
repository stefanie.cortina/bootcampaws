package com.microservicioaws.infrastructure.exception;

public class DataEmptyException extends RuntimeException{
    public DataEmptyException() {
        super();
    }
}
