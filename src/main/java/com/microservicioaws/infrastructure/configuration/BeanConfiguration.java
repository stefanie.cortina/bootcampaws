package com.microservicioaws.infrastructure.configuration;

import com.microservicioaws.domain.logic.ProductoOperations;
import com.microservicioaws.domain.logic.ProductoPersistence;
import com.microservicioaws.domain.usecase.ProductoUseCase;
import com.microservicioaws.infrastructure.persistence.adapter.ProductoJpaAdapter;
import com.microservicioaws.infrastructure.persistence.entity.ProductoEntity;
import com.microservicioaws.infrastructure.persistence.mapper.ProductoEntityMapper;
import com.microservicioaws.infrastructure.persistence.repository.JpaProductoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackageClasses = JpaProductoRepository.class)
@EntityScan(basePackageClasses = ProductoEntity.class)
@RequiredArgsConstructor
public class BeanConfiguration {
    private final JpaProductoRepository productoRepository;
    private final ProductoEntityMapper productoEntityMapper;

    @Bean
    public ProductoPersistence productoPersistence(){
        return new ProductoJpaAdapter(productoRepository, productoEntityMapper);
    }

    @Bean
    public ProductoOperations productoOperations(){
        return new ProductoUseCase(productoPersistence());
    }
}
