package com.microservicioaws.infrastructure.exceptionhandler;

public enum ExceptionResponse {
    PRODUCT_NOT_FOUND("Producto no encontrado"),
    DATA_EMPTY_FOUND("Campos vacios");

    private String message;

    ExceptionResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }
}
