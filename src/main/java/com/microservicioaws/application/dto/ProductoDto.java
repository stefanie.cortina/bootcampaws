package com.microservicioaws.application.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductoDto {
    private String nombre;
    private Double precio;
}
