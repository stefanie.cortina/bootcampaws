package com.microservicioaws.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.microservicioaws"})
public class MicroservicioAwsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroservicioAwsApplication.class, args);
	}

}
