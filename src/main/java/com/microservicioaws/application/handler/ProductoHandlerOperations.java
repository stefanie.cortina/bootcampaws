package com.microservicioaws.application.handler;

import com.microservicioaws.application.dto.ProductoDto;

import java.util.List;

public interface ProductoHandlerOperations {

    ProductoDto getById(Long id);
    ProductoDto saveProducto(ProductoDto producto);
    List<ProductoDto> findAllProducto();
    ProductoDto update(ProductoDto producto, Long id);
    void delete(Long id);
}
