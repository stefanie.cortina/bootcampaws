package com.microservicioaws.application.handler;

import com.microservicioaws.application.dto.ProductoDto;
import com.microservicioaws.application.mapper.ProductoDtoMapper;
import com.microservicioaws.domain.logic.ProductoOperations;
import com.microservicioaws.domain.model.Producto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class ProductoHandler implements ProductoHandlerOperations{

    private final ProductoOperations productoOperations;
    private final ProductoDtoMapper productoDtoMapper;

    @Override
    public ProductoDto getById(Long id) {
        Producto producto = productoOperations.getById(id);
        return productoDtoMapper.toProductoDto(producto);
    }

    @Override
    public ProductoDto saveProducto(ProductoDto productoDto) {
        Producto producto = productoDtoMapper.toProducto(productoDto);
        productoOperations.saveProducto(producto);
        return productoDto;
    }

    @Override
    public List<ProductoDto> findAllProducto() {
        return productoDtoMapper.toProductoDtoList(productoOperations.findAllProducto());
    }

    @Override
    public ProductoDto update(ProductoDto productoDto, Long id) {
        Producto getProducto = productoOperations.getById(id);

        Producto newProducto = productoDtoMapper.toProducto(productoDto);
        newProducto.setId(getProducto.getId());
        newProducto.setNombre(getProducto.getNombre());
        newProducto.setPrecio(getProducto.getPrecio());
        productoOperations.update(newProducto);

        return productoDto;
    }

    @Override
    public void delete(Long id) {
        productoOperations.delete(id);
    }
}
