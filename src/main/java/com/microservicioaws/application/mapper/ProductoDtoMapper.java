package com.microservicioaws.application.mapper;

import com.microservicioaws.application.dto.ProductoDto;
import com.microservicioaws.domain.model.Producto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductoDtoMapper {

    Producto toProducto(ProductoDto productoDto);
    ProductoDto toProductoDto(Producto producto);

    default List<ProductoDto> toProductoDtoList(List<Producto> productoList) {
        return productoList.stream()
                .map(producto -> {
                    ProductoDto productoDto = new ProductoDto();
                    productoDto.setNombre(producto.getNombre());
                    productoDto.setPrecio(producto.getPrecio());
                    return productoDto;
                }).toList();
    }
}
