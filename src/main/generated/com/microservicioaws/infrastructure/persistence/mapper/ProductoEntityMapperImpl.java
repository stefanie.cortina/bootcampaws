package com.microservicioaws.infrastructure.persistence.mapper;

import com.microservicioaws.domain.model.Producto;
import com.microservicioaws.infrastructure.persistence.entity.ProductoEntity;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-10-19T17:56:03-0500",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 17.0.6 (Amazon.com Inc.)"
)
@Component
public class ProductoEntityMapperImpl implements ProductoEntityMapper {

    @Override
    public ProductoEntity toEntity(Producto producto) {
        if ( producto == null ) {
            return null;
        }

        ProductoEntity productoEntity = new ProductoEntity();

        productoEntity.setId( producto.getId() );
        productoEntity.setNombre( producto.getNombre() );
        productoEntity.setPrecio( producto.getPrecio() );

        return productoEntity;
    }

    @Override
    public Producto toProducto(ProductoEntity productoEntity) {
        if ( productoEntity == null ) {
            return null;
        }

        Producto producto = new Producto();

        producto.setId( productoEntity.getId() );
        producto.setNombre( productoEntity.getNombre() );
        producto.setPrecio( productoEntity.getPrecio() );

        return producto;
    }

    @Override
    public List<Producto> toProductoList(List<ProductoEntity> productoEntityList) {
        if ( productoEntityList == null ) {
            return null;
        }

        List<Producto> list = new ArrayList<Producto>( productoEntityList.size() );
        for ( ProductoEntity productoEntity : productoEntityList ) {
            list.add( toProducto( productoEntity ) );
        }

        return list;
    }
}
