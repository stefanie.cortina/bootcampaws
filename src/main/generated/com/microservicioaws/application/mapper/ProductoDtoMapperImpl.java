package com.microservicioaws.application.mapper;

import com.microservicioaws.application.dto.ProductoDto;
import com.microservicioaws.domain.model.Producto;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-10-19T17:56:04-0500",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 17.0.6 (Amazon.com Inc.)"
)
@Component
public class ProductoDtoMapperImpl implements ProductoDtoMapper {

    @Override
    public Producto toProducto(ProductoDto productoDto) {
        if ( productoDto == null ) {
            return null;
        }

        Producto producto = new Producto();

        producto.setNombre( productoDto.getNombre() );
        producto.setPrecio( productoDto.getPrecio() );

        return producto;
    }

    @Override
    public ProductoDto toProductoDto(Producto producto) {
        if ( producto == null ) {
            return null;
        }

        ProductoDto productoDto = new ProductoDto();

        productoDto.setNombre( producto.getNombre() );
        productoDto.setPrecio( producto.getPrecio() );

        return productoDto;
    }
}
