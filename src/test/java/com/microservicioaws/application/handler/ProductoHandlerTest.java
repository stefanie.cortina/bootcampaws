package com.microservicioaws.application.handler;

import com.microservicioaws.application.dto.ProductoDto;
import com.microservicioaws.application.mapper.ProductoDtoMapper;
import com.microservicioaws.domain.logic.ProductoOperations;
import com.microservicioaws.domain.model.Producto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProductoHandlerTest {

    @InjectMocks
    private ProductoHandler productoHandler;

    @Mock
    private ProductoOperations productoOperations;

    @Mock
    private ProductoDtoMapper productoDtoMapper;

    @Test
    public void testGetById() {
        // Arrange
        Long productId = 1L;
        Producto producto = new Producto(productId,"product1",123.0);
        ProductoDto productoDto = new ProductoDto();

        when(productoOperations.getById(productId)).thenReturn(producto);
        when(productoDtoMapper.toProductoDto(producto)).thenReturn(productoDto);

        // Act
        ProductoDto result = productoHandler.getById(productId);

        // Assert
        verify(productoOperations).getById(productId);
        verify(productoDtoMapper).toProductoDto(producto);
        assertEquals(productoDto, result);
    }

    @Test
    public void testSaveProducto() {
        // Arrange
        ProductoDto productoDto = new ProductoDto();
        Producto producto = new Producto(1L,"product1",123.0);

        when(productoDtoMapper.toProducto(productoDto)).thenReturn(producto);

        // Act
        ProductoDto result = productoHandler.saveProducto(productoDto);

        // Assert
        verify(productoDtoMapper).toProducto(productoDto);
        verify(productoOperations).saveProducto(producto);
        assertEquals(productoDto, result);
    }

    @Test
    public void testFindAllProducto() {
        // Arrange
        List<Producto> productos = Arrays.asList(new Producto(1L,"product1",123.0),
                new Producto(2L,"product2",145.0));
        List<ProductoDto> productoDtos = Arrays.asList(new ProductoDto(), new ProductoDto());

        when(productoOperations.findAllProducto()).thenReturn(productos);
        when(productoDtoMapper.toProductoDtoList(productos)).thenReturn(productoDtos);

        // Act
        List<ProductoDto> result = productoHandler.findAllProducto();

        // Assert
        verify(productoOperations).findAllProducto();
        verify(productoDtoMapper).toProductoDtoList(productos);
        assertEquals(productoDtos, result);
    }

    @Test
    public void testUpdate() {
        // Arrange
        Long productId = 1L;
        Producto getProducto = new Producto(productId,"product1",123.0);
        ProductoDto productoDto = new ProductoDto();
        Producto newProducto = new Producto(productId,"product1",147.3);

        when(productoOperations.getById(productId)).thenReturn(getProducto);
        when(productoDtoMapper.toProducto(productoDto)).thenReturn(newProducto);

        // Act
        ProductoDto result = productoHandler.update(productoDto, productId);

        // Assert
        verify(productoOperations).getById(productId);
        verify(productoDtoMapper).toProducto(productoDto);
        verify(productoOperations).update(newProducto);
        assertEquals(productoDto, result);
    }

    @Test
    public void testDelete() {
        // Arrange
        Long productId = 1L;
        Producto producto = new Producto(productId,"product1",123.0);

        when(productoOperations.getById(productId)).thenReturn(producto);

        // Act
        productoHandler.delete(productId);

        // Assert
        verify(productoOperations).getById(productId);
        verify(productoOperations).delete(productId);
    }
}