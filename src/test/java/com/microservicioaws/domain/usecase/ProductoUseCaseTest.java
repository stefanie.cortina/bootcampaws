package com.microservicioaws.domain.usecase;

import com.microservicioaws.domain.logic.ProductoPersistence;
import com.microservicioaws.domain.model.Producto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProductoUseCaseTest {
    @Mock
    private ProductoPersistence productoPersistence;

    private ProductoUseCase productoUseCase;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        productoUseCase = new ProductoUseCase(productoPersistence);
    }

    @Test
    public void testGetById() {
        Long id = 1L;
        Producto expectedProducto = new Producto(id, "Producto 1", 123.0);

        when(productoPersistence.getById(id)).thenReturn(expectedProducto);

        Producto result = productoUseCase.getById(id);

        assertEquals(expectedProducto, result);
    }

    @Test
    public void testSaveProducto() {
        Producto producto = new Producto(1L,"Nuevo Producto",123.0);
        productoUseCase.saveProducto(producto);

        verify(productoPersistence).saveProducto(producto);
    }

    @Test
    public void testFindAllProducto() {
        List<Producto> expectedProductos = new ArrayList<>();
        expectedProductos.add(new Producto(1L, "Producto 1",123.0));
        expectedProductos.add(new Producto(2L, "Producto 2",123.0));

        when(productoPersistence.findAllProducto()).thenReturn(expectedProductos);

        List<Producto> result = productoUseCase.findAllProducto();

        assertEquals(expectedProductos, result);
    }

    @Test
    public void testUpdate() {
        Producto producto = new Producto(1L, "Producto Actualizado", 123.0);
        productoUseCase.update(producto);

        verify(productoPersistence).update(producto);
    }

    @Test
    public void testDelete() {
        Long id = 1L;
        productoUseCase.delete(id);

        verify(productoPersistence).delete(id);
    }
}