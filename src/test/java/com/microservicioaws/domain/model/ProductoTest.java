package com.microservicioaws.domain.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
public class ProductoTest {

    @Test
    public void testConstructorYGetters() {
        // Arrange
        Long id = 1L;
        String nombre = "Producto de prueba";
        Double precio = 19.99;

        // Act
        Producto producto = new Producto(id, nombre, precio);

        // Assert
        assertEquals(id, producto.getId());
        assertEquals(nombre, producto.getNombre());
        assertEquals(precio, producto.getPrecio());
    }

    @Test
    public void testSetters() {
        // Arrange
        Producto producto = new Producto(1L, "Producto inicial", 10.0);

        // Act
        producto.setId(2L);
        producto.setNombre("Nuevo producto");
        producto.setPrecio(15.0);

        // Assert
        assertEquals(2L, producto.getId());
        assertEquals("Nuevo producto", producto.getNombre());
        assertEquals(15.0, producto.getPrecio(), 0.001); // Aseguramos tolerancia para números de punto flotante
    }

}