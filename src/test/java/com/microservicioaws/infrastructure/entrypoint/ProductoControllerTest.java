package com.microservicioaws.infrastructure.entrypoint;

import com.microservicioaws.application.dto.ProductoDto;
import com.microservicioaws.application.handler.ProductoHandlerOperations;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class ProductoControllerTest {

    @InjectMocks
    private ProductoController productoController;

    @Mock
    private ProductoHandlerOperations productoHandlerOperations;

    @Test
    void guardarProductoDeberiaRetornarHttpStatusCreated() {
        // Configuración de datos de prueba
        ProductoDto productoDto = new ProductoDto();

        // Configuración del comportamiento del mock
        Mockito.when(productoHandlerOperations.saveProducto(productoDto)).thenReturn(productoDto);

        // Ejecución de la prueba
        ResponseEntity<Void> response = productoController.guardarProducto(productoDto);

        // Verificación de resultados
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
    }

    @Test
    void getAllProductosDeberiaRetornarUnaListaDeProductos() {
        // Configuración del comportamiento del mock
        Mockito.when(productoHandlerOperations.findAllProducto()).thenReturn(Collections.emptyList());

        // Ejecución de la prueba
        ResponseEntity<List<ProductoDto>> response = productoController.getAllProductos();

        // Verificación de resultados
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(Collections.emptyList(), response.getBody());
    }

    @Test
    void getProductoByIdDeberiaRetornarUnProducto() {
        // Configuración de datos de prueba
        Long idProducto = 1L;
        ProductoDto productoDto = new ProductoDto();

        // Configuración del comportamiento del mock
        Mockito.when(productoHandlerOperations.getById(idProducto)).thenReturn(productoDto);

        // Ejecución de la prueba
        ResponseEntity<ProductoDto> response = productoController.getProductoById(idProducto);

        // Verificación de resultados
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(productoDto, response.getBody());
    }

    @Test
    void updateProductoDeberiaRetornarHttpStatusNoContent() {
        // Configuración de datos de prueba
        Long idProducto = 1L;
        ProductoDto productoDto = new ProductoDto();

        // Ejecución de la prueba
        ResponseEntity<Void> response = productoController.updateProducto(productoDto, idProducto);

        // Verificación de resultados
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    void deleteProductoDeberiaRetornarHttpStatusNoContent() {
        // Configuración de datos de prueba
        Long idProducto = 1L;

        // Ejecución de la prueba
        ResponseEntity<Void> response = productoController.deleteProducto(idProducto);

        // Verificación de resultados
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

}