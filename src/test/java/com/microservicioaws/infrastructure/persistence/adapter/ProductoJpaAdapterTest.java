package com.microservicioaws.infrastructure.persistence.adapter;

import com.microservicioaws.domain.model.Producto;
import com.microservicioaws.infrastructure.exception.DataEmptyException;
import com.microservicioaws.infrastructure.exception.ProductNotFoundException;
import com.microservicioaws.infrastructure.persistence.entity.ProductoEntity;
import com.microservicioaws.infrastructure.persistence.mapper.ProductoEntityMapper;
import com.microservicioaws.infrastructure.persistence.repository.JpaProductoRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProductoJpaAdapterTest {

    @InjectMocks
    private ProductoJpaAdapter productoJpaAdapter;

    @Mock
    private JpaProductoRepository productoRepository;

    @Mock
    private ProductoEntityMapper productoEntityMapper;

    @Test
    void testGetById() {
        Long id = 1L;
        ProductoEntity productoEntity = new ProductoEntity();
        Producto producto = new Producto();
        producto.setId(id);
        producto.setNombre("Producto 1");
        producto.setPrecio(10.0);
        productoEntity.setId(id);
        when((productoRepository.findById(any()))).thenReturn(Optional.of((productoEntity)));
        when(productoEntityMapper.toProducto(productoEntity)).thenReturn(producto);

        Producto byId = productoJpaAdapter.getById(anyLong());
        assertNotNull(byId);
        verify(productoRepository, times(1)).findById(anyLong());
        assertEquals(id, producto.getId());
        assertEquals("Producto 1", byId.getNombre());
        assertEquals(10.0, byId.getPrecio(), 0.001);

    }

    @Test
    void testGetById_ProductNotFoundException() {
        Long id = 1L;
        when(productoRepository.findById(id)).thenReturn(Optional.empty());

        assertThrows(ProductNotFoundException.class, () -> {
            productoJpaAdapter.getById(id);
        });
    }

    @Test
    void testSaveProducto() {
        Producto producto = new Producto();
        producto.setNombre("Producto 1");
        producto.setPrecio(10.0);
        ProductoEntity productoEntity = new ProductoEntity();
        when(productoEntityMapper.toEntity(producto)).thenReturn(productoEntity);

        productoJpaAdapter.saveProducto(producto);

        verify(productoRepository, times(1)).save(productoEntity);
    }

    @Test
    void testSaveProducto_DataEmptyException() {
        Producto producto = new Producto();
        producto.setId(1L);
        assertThrows(DataEmptyException.class, () -> {
            productoJpaAdapter.saveProducto(producto);
        });
    }

    @Test
    void testFindAllProducto() {
        List<ProductoEntity> productoEntities = Arrays.asList(new ProductoEntity(1L,"product1",123.0),
                new ProductoEntity(2L,"product2",145.0));

        Producto producto = new Producto();

        when(productoRepository.findAll()).thenReturn(productoEntities);
        when(productoEntityMapper.toProductoList(productoEntities)).thenReturn(Arrays.asList(producto));

        List<Producto> productos = productoJpaAdapter.findAllProducto();

        assertFalse(productos.isEmpty());
        assertEquals(1, productos.size());
    }

    @Test
    void testFindAllProducto_ProductNotFoundException() {
        when(productoRepository.findAll()).thenReturn(List.of());

        assertThrows(ProductNotFoundException.class, () -> {
            productoJpaAdapter.findAllProducto();
        });
    }

    @Test
    void testUpdate() {
        Long id = 1L;
        Producto producto = new Producto();
        producto.setId(id);
        producto.setNombre("Producto Actualizado");
        producto.setPrecio(15.0);
        ProductoEntity productoEntity = new ProductoEntity();
        productoEntity.setId(producto.getId());
        productoEntity.setNombre(producto.getNombre());
        when(productoRepository.findById(id)).thenReturn(Optional.of(productoEntity));

        productoJpaAdapter.update(producto);

        verify(productoRepository, times(1)).save(productoEntity);
    }

    @Test
    void testUpdate_ProductNotFoundException() {
        Long id = 1L;
        Producto producto = new Producto();
        producto.setId(id);
        assertThrows(ProductNotFoundException.class, () -> {
            productoJpaAdapter.update(producto);
        });
    }

    @Test
    void testDelete() {

        Long id = 1L;
        ProductoEntity productoEntity = new ProductoEntity();
        when(productoRepository.findById(id)).thenReturn(Optional.of(productoEntity));
        productoJpaAdapter.delete(id);
        verify(productoRepository, times(1)).deleteById(id);
    }

    @Test
    void testDelete_ProductNotFoundException() {
        Long id = 1L;
        assertThrows(ProductNotFoundException.class, () -> {
            productoJpaAdapter.delete(id);
        });
    }
}