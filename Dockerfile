FROM openjdk:17
EXPOSE 8090
RUN mkdir /app
# ARG JAR_FILE=build/libs/*.jar
COPY build/libs/*.jar /app/
ENTRYPOINT ["java", "-jar", "/app/microservicio-aws-0.0.1-SNAPSHOT.jar"]

#comandos dockerfile
# se configura el dockerfile enconsola se ubica en la carpeta del micro y se ejecutan estoscomandos
# cd /mnt/c/PRAGMA/bootcampaws$   C:\PRAGMA\bootcampaws
# comando....
# docker build -t ecr-microaws .
# verificar que se creo la imagen
# docker image ls
# ejecutar el micro desde la imagen
# docker run -p 9090:8090 microaws.jar    failed
# docker run --net=host -p 8090:8090 15901a402487      exitoso
# en otra ventada
# docker ps -a
# docker stop 3489d95554a0



